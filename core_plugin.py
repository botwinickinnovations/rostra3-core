from envisage.core_plugin import CorePlugin as _CorePlugin

from rostra3.misc import rostra_envisage_combined_versions as _version
from rostra3.plugins import BasePlugin as _Plugin


# noinspection PyClassHasNoInit
class CorePlugin(_CorePlugin, _Plugin):
    name = 'Core Plugin'
    version = _version()
    gui = False
