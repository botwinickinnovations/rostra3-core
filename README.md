# README #

I need to write all the documentation and notes, but... Rostra 3 is essentially
an opinionated set of extensions to the Entought Tool Suite Base (envisage, pyface, traitsui, traits, etc.)
The Enthought Tool Suite is a really cool base for writing software quickly (i.e. spending more time focusing
on the application itself and less time on the infrastructure that supports it.) However, I still felt the 
need to tinker with it.

Some of the work on Rostra 3 that falls into the categories of bug fixes and/or extensions that wouldn't force
Rostra's "opinions" upstream should be (and will be at some point) contributed back up to envisage/pyface/etc.
It's possible in the long-run that Rostra 3 could disintegrate if the majority of its functionality ends up
moving up to envisage--we'll see how things turn out. In that case, Rostra 3 would be less of a platform
and more of a collection of useful envisage plugins.

Rostra 3 is meant to be a platform for writing plugin-based GUI or CLI applications that use Python for the UI
layer. Rostra 3 enforces the use of Qt (not Qt or Wx) and also--PySide is preferred. One of the design goals
of Rostra is to form a base for software/tools that are ready to be commercialized. All of the core Rostra 
components are built (or intended to be built) with support for 'white-box branding', i.e. it won't say 
Rostra everywhere, it'll say whatever you want it to say...

Rostra 3 is also designed to force some good practices on less experienced programmers (work in progress).
Example: don't run calculations in the UI thread. Rostra 3 is ETS but includes some basic threaded task management
etc. 

As mentioned earlier, Rostra 3 is an opinionated version of ETS, so some of its extensions aren't easily compatible to
ETS; however, it is intended that envisage plugins can be used in Rostra applications without modification. Some of 
the core components of Rostra 3 are actually just updated / improved / fixed versions of ETS plugins. (Again, some of
these may end up being contributed upstream.) [For example, the IPython Shell included as part of envisage
is incompatbile with newer IPython versions since the Jupyter transition.]

Rostra 3 and its associated bootstrap components are compatible with being used with Cython to create native/compiled libraries.
(Some workarounds are necessary to get the stack frames right for traits if the WHOLE application [including Rostra 3 itself] is
cythonized.)

Why 3? You never heard of Rostra 2? ....Well probably not since it was never released to anyone but me. Rostra 2 was a similar
platform but implemented in Java (i.e. it wasn't based on ETS). ETS is actually fairly similar to Rostra 2 except that Rostra 2
was written in Java and technically a less flexible framework and also included some common data components to facilitate sharing
data between plugins without the plugins having any prior knowledge of each other. I might end up creating similar functionality 
as a set of extensions to Rostra 3 in the future. I used Rostra 2 as the foundation for some of my internal tools because Eclipse 
stuff and OSGi were a pain and I didn't want to keep doing that stuff over and over, but I saw the value in having it. Both ETS and Rostra 2
took inspiration from Java libraries, OSGi, etc.; however, Rostra 2 clearly retained more influenced from Eclipse and OSGi than ETS. Rostra 3
will add some additional parts inspired by OSGi etc. (Example: very simple dependency structure in plugins so that the order in which plugins
are started and stopped is logical.)

Anyway, I started work on Rostra 3 because I transitioned to using Python more and wanted a similar kind of base to make
it easier to develop applications. At first, my plan was to write everything myself, but I found that ETS was fairly similar,
so I thought it'd make sense to use that. Once I got into it, I found that ETS (envisage+) lacked some of the features I was expecting
and/or it seems like it's not maintained as actively as I'd like. (Not that it's not maintained... ) ...But regardless, I ended up forcing 
opinions and fixes on top of ETS and decided it made sense to call that Rostra 3... the successor to Rostra 2. 


### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact