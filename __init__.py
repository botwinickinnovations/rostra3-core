from os import path as osp

ID = 'rostra3.platform'
CORE_LOGGER_NAME = 'platform'
PATH = osp.abspath(osp.dirname(__file__))

R3_BIN_DIR = None
R3_PLUGINS_DIR = None

version = '3.0.0'  # TODO: proper version tracking mechanism(s)
