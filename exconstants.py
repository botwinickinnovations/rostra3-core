# Core Plugin References
PLUGIN_APP_CORE = 'envisage.core'
PLUGIN_APP_WORKBENCH = 'envisage.ui.workbench'

# Envisage / ETS Standard Extension Points
EXT_VIEWS = 'envisage.ui.workbench.views'
EXT_PERSPECTIVES = 'envisage.ui.workbench.perspectives'
EXT_PREFERENCES_PAGES = 'envisage.ui.workbench.preferences_pages'
EXT_PREFERENCES = 'envisage.preferences'
EXT_SERVICE_OFFERS = 'envisage.service_offers'
EXT_ACTION_SETS = 'envisage.ui.workbench.action_sets'

# Envisage / ETS Plugin Extension Points
EXT_IPYTHON_BANNER = 'envisage.plugins.ipython_shell.banner'

# Envisage / ETS View IDs
VIEW_SHELL = 'envisage.plugins.python_shell_view'
VIEW_LOGGER = 'apptools.logger.plugin.view.logger_view.LoggerView'
