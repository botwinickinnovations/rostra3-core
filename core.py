# encoding: utf-8
# cython: embedsignature=False, linetrace=False, profile=False, emit_code_comments=False
__compiler__ = 'cython'

# region Imports

import logging
import platform
import sys
from getpass import getuser as _user_name
from logging.handlers import TimedRotatingFileHandler as _TRFileHandler
from os import path as osp

import envisage.plugin_manager
import psutil as psu
from envisage.api import IApplication as _IApplication, Application as _Application
from envisage.plugin_manager import PluginManager as _PluginManager
from traits.api import HasTraits, Instance, Int, on_trait_change
from traits.etsconfig.api import ETSConfig

from . import CORE_LOGGER_NAME, ID, PATH, R3_BIN_DIR, R3_PLUGINS_DIR, exconstants as xc
from .util import class_name


# endregion

# region Useful functions

def _user_home_dir(user=''):
    return osp.expanduser('~%s' % user)


def _operating_system():
    try:
        return platform.system().lower()  # this only works when there are file descriptors attached for stdout????
    except IOError:
        try:
            from win32process import DETACHED_PROCESS as _
            return 'windows'
        except ImportError:
            _ = None
            return 'linux'


def _init_logging(log_level, file_name=None, days_to_keep=7, basic=False):
    # Logging Configuration
    fs = '%(asctime)s %(levelname)s %(threadName)s %(name)s > %(message)s'  # %(funcName)s()
    dfs = '%Y/%m/%d %H:%M:%S'
    if file_name and not basic:
        formatter = logging.Formatter(fs, dfs)
        today_handler = _TRFileHandler(file_name, when='D', backupCount=0, utc=True)
        today_handler.setFormatter(formatter)
        logging.root.addHandler(today_handler)
        archive_handler = _TRFileHandler('%s.gz' % file_name, when='D', backupCount=days_to_keep, utc=True,
                                         encoding='zlib')
        archive_handler.setFormatter(formatter)
        logging.root.addHandler(archive_handler)
        logging.root.setLevel(log_level)
    else:
        logging.basicConfig(format=fs, datefmt=dfs, level=log_level, filename=file_name)


def _log_welcome(log, alt_name=None):
    log.info('Initializing %s' % (alt_name if alt_name is not None else 'Rostra 3 Platform'))

    # version information
    # from .version import version
    # logger.info(__bootstrap_log_fmt, 'Version', version)

    # operating system
    log.info(_bootstrap_log_fmt, 'Operating System', OPERATING_SYSTEM.title())

    # machine & processor information
    machine, processor = platform.machine(), platform.processor()
    log.info(_bootstrap_log_fmt, 'Processor',
             ('%dL (%dP)' % (psu.cpu_count(logical=True), psu.cpu_count(logical=False))) +
             '%s cores [%s]' % ((' ' + machine) if machine != processor else '', processor))

    # memory information
    log.info(_bootstrap_log_fmt, 'Memory', '%.0f GB RAM' %
             (psu.virtual_memory().total / (1024.0 * 1024.0 * 1024.0)))

    pass


# endregion

# region Core Global Variables

_bootstrap_log_fmt = '%s = %s'

# assign variables and remove access to functions
USER_NAME = _user_name = _user_name()
USER_HOME_DIRECTORY = _user_home_dir = _user_home_dir()
OPERATING_SYSTEM = _operating_system = _operating_system()

ASSETS_DIR = osp.abspath(osp.join(PATH, 'assets'))
EFFECTIVE_APPLICATION_ID = ID
GUI_ENABLED = True
R3_BIN_DIR = R3_BIN_DIR  # silly...

logger = logging.getLogger(CORE_LOGGER_NAME)
R3_INSTANCE = None


# endregion


# noinspection PyClassHasNoInit
class RostraApplication(_Application):
    task_manager = Instance('rostra3.qt4.task_manager.SimpleTaskManager')  # type: SimpleTaskManager
    """
    Task Manager is part of all Rostra Applications. Tasks should be queued with
    the same approach with or without a GUI. Displaying progress will obviously occur
    differently between TUI and GUI applications, but there is no reason why the same
    mechanisms and code shouldn't be available in any application in any context.

    Note that the current task manager implementation uses Qt4. This is based
    on the intended design that all Rostra applications are based on Qt4/PySide.
    If Rostra grows at some point, it is possible that dependency will be dropped
    in favor of just using a traits and threads based approach so that there is no
    forced dependency on Qt4, but for the moment, that's how it is. Just be aware.
    (Despite the dependency on Qt4, the Task Manager should NOT depend on QtGui and
    therefore should work equally well on Text or Graphical Interfaces...)
    """

    # noinspection PyMethodMayBeStatic
    def _task_manager_default(self):
        from .qt4.task_manager import SimpleTaskManager
        # TODO: smartly determine the simultaneous tasks limit/allotment
        return SimpleTaskManager(4)

    def queue_task(self, task):
        self.task_manager.queue(task)

    pass


class RostraPlatform(HasTraits):
    app = Instance(_IApplication)
    plugin_manager = Instance(_PluginManager)
    preferences = Instance('apptools.preferences.api.IPreferences')
    log_level = Int(logging.INFO)

    def main(self, env):
        # override ETS company before other initialization
        ETSConfig.company = env['company_name'] if 'company_name' in env and env['company_name'] else 'RostraPlatform'

        # self.parse_command_line(argv if argv is not None else [])  # parse command line arguments if applicable

        # initialize logger (might make use of command line arguments?)
        self._init_logger(env['log_level'] if 'log_level' in env else None)
        self._configure_warnings()  # configure warnings after logger and command line options, before everything else

        # log welcome statements unless explicitly suppressed
        if 'suppress_welcome' not in env or not env['suppress_welcome']:
            # Log some welcome INFO statements
            _log_welcome(logger, alt_name=env['app_title'] if 'app_title' in env else None)

        if 'force_cli' in env and env['force_cli']:
            global GUI_ENABLED  # TODO: get rid of global assignment like this? although it is convenient...
            GUI_ENABLED = False
            logger.info('Graphical User Interface Disabled')

        if 'main_assets_dir' in env and env['main_assets_dir']:
            global ASSETS_DIR
            ASSETS_DIR = osp.abspath(env['main_assets_dir'])

        # TODO: make use of env['additional_assets_dirs'] if defined
        # TODO: assets directories should form a cascade (provided main, provided additional, default main, etc.)

        # Initialize the Plugin Manager (might make use of command line arguments?)
        plugins = None
        self._init_plugin_manager(plugins,
                                  additional_plugin_paths=env['additional_plugins_dirs']
                                  if 'additional_plugins_dirs' in env else None,
                                  includes=env['plugins_match_include'] if 'plugins_match_include' in env else None,
                                  excludes=env['plugins_match_exclude'] if 'plugins_match_exclude' in env else None)

        # Initialize Preferences Manager
        self._init_preferences()  # initialize preferences / settings after plugin manager init

        # Start GUI
        if GUI_ENABLED:
            self._gui_init(env)
        else:  # FORCE CLI
            self._tui_init(env)

    def _gui_init(self, options, start_event_loop=True):
        logger.info('Initializing GUI')

        # Force the selection of a toolkit; we need to fail now if we're going to fail...
        self._init_toolkit()

        # init the actual application using the components
        self._init_workbench(options, start_event_loop=start_event_loop)

        self._start_application()

    def _tui_init(self, options):
        logger.info('Initializing TUI')
        from .upstream.envisage.extensions import ServiceRegistry

        app = RostraApplication(
            plugin_manager=self.plugin_manager,
            service_registry=ServiceRegistry(),
            # preferences=self.preferences,
        )
        self.app = app

        app.on_trait_change(self._on_app_tui_started, 'started')  # register app gui started trait change notifier

        self._start_application()

    def _start_application(self):  # separate_gui_thread=True
        logger.info('Starting Application')
        self.app.run()

    def _init_workbench(self, options, plugins=None, start_event_loop=True):
        logger.info('Initializing Workbench')
        from .widgets import RostraWorkbenchApp
        from .upstream.envisage.extensions import ServiceRegistry

        # if 'disable_perspectives_ui' in options:
        #     import pyface.workbench.action.view_menu_manager
        #     pyface.workbench.action.view_menu_manager.ViewMenuManager.show_perspective_menu = \
        #         Bool(not options['disable_perspectives_ui'])

        # if 'default_perspective' in options and options['default_perspective']:
        #     import pyface.workbench.workbench_window
        #     pyface.workbench.workbench_window.WorkbenchWindow.default_perspective_id = options['default_perspective']

        extra_opts = {}
        if 'app_title' in options and options['app_title']:
            extra_opts['name'] = options['app_title']
        # if 'disable_perspectives_ui' in options:
        #     extra_opts['show_perspective_menu'] = not options['disable_perspectives_ui']
        # if 'default_perspective' in options and options['default_perspective']:
        #     extra_opts['default_perspective'] = options['default_perspective']

        app = RostraWorkbenchApp(
            plugin_manager=self.plugin_manager,
            service_registry=ServiceRegistry(),
            plugins=plugins,
            preferences=self.preferences,
            start_gui_event_loop=start_event_loop,
            **extra_opts
        )
        self.app = app

        app.gui.on_trait_change(self._on_app_gui_started, 'started')  # register app gui started trait change notifier

        return app

    def _init_preferences(self):
        if GUI_ENABLED:
            logger.info('Initializing Preferences Manager')
            from rostra3.preferences import preference_manager
            self.preferences = preference_manager.preferences
        else:
            logger.warn('Skipping Preferences Manager because GUI disabled. Non-GUI Preferences not yet implemented.')

    # noinspection PyMethodMayBeStatic
    def _init_toolkit(self):
        logger.debug('_init_toolkit(): Starting...')
        try:
            from traitsui.api import toolkit
            toolkit = toolkit()
        except Exception as e:
            logger.error('_init_toolkit(): Exception: %s', e)
            raise e

        try:
            logger.debug(_bootstrap_log_fmt, '_init_toolkit(): GUI Toolkit', str(type(toolkit)).split('.')[1])
        except Exception as e:
            logger.error('_init_toolkit(): Error identifying name of GUI toolkit: %s', e)

    def _init_plugin_manager(self, plugins=None, additional_plugin_paths=None, includes=None, excludes=None):
        logger.info('Initializing Plugin Manager')
        plugin_paths = [R3_PLUGINS_DIR]
        if additional_plugin_paths:
            plugin_paths.extend(additional_plugin_paths)

        from .plugins import R3PluginManager

        # monkey patch ETS plugin manager logger # TODO: do this in a cleaner way?
        envisage.plugin_manager.logger = R3PluginManager.logger

        self.plugin_manager = R3PluginManager(plugin_path=plugin_paths, plugins=plugins,
                                              include=includes or [], exclude=excludes or [])

    def _init_logger(self, log_mode=None):
        if log_mode is not None:
            self.log_level = log_mode

        # external logging init routine (TODO: fold into this class?)
        _init_logging(self.log_level)

    # noinspection PyMethodMayBeStatic
    def _configure_warnings(self):
        from warnings import simplefilter
        simplefilter(action='ignore', category=DeprecationWarning)
        simplefilter(action='ignore', category=FutureWarning)
        # TODO: we may want to show some of these warnings when in debug more or showing debug logging messages
        if self.log_level > logging.DEBUG:
            simplefilter(action='ignore', category=RuntimeWarning)
            simplefilter(action='ignore', category=UserWarning)

    # noinspection PyMethodMayBeStatic
    def run(self):
        logger.info('Application Started')

    ######################################################################
    # Non-public interface.
    ######################################################################
    def _on_app_tui_started(self, obj, trait_name, old, new):
        if trait_name != 'started' or not new:
            return
        app = self.app

        # TODO: force add an additional plugin to the plugin manager that makes CLI stuff happen?
        # TODO: publish CLI arguments via an extension, have plugins optionally consume those...?
        logger.info('Application w/ TUI initialized... but....')
        logger.error('CLI interface approach is not yet implemented...')

        # self.run()  # TODO: should this be 'invoked later', use different thread? etc.
        pass

    def _on_app_gui_started(self, obj, trait_name, old, new):
        """This is called as soon as  the Envisage GUI starts up.  The
        method is responsible for setting our script instance.
        """
        if trait_name != 'started' or not new:
            return
        app = self.app

        # TODO: fix delayed shell binding
        # TODO: binding base application variables etc should be done on start-up of the python shell not application
        window = self.app.workbench.active_window
        py = window.get_view_by_id(xc.VIEW_SHELL)
        if py is None:
            logger.warn("Can't find the Python shell view to bind variables")
        else:
            try:
                py.bind('application', app)
                py.bind('app', app)
            except AttributeError:
                logger.warn('Unable to bind application variables to python shell')

        app.gui.invoke_later(self.run)

    @on_trait_change('app.service_registry:registered')
    def _on_app_service_registered(self, obj, name, old, new):
        if name != 'registered' or not new:
            return
        service = obj.resolve_service_from_id(new)

        clazz_name = class_name(service)
        logger.debug('_on_app_service_registered(): Checking for start() for service: %s', clazz_name)

        if hasattr(service, 'start'):
            logger.debug('_on_app_service_registered(): Calling start() for service: %s', clazz_name)
            service.start()

    @on_trait_change('app.service_registry:unregistered')
    def _on_app_service_unregistered(self, obj, name, old, new):
        if name != 'unregistered' or not new:
            return
        try:
            service = obj.resolve_service_from_id(new)
            clazz_name = class_name(service)
            logger.debug('_on_app_service_unregistered(): Checking for stop() for service: %s', clazz_name)

            if hasattr(service, 'stop'):
                logger.debug('_on_app_service_unregistered(): Calling stop() for service: %s', clazz_name)
                service.stop()
        except ValueError:
            # TODO: this might be a problem if we're not on app shutdown AND service unregister matters (unlikely)
            # to be clear, problem is that unregistered notification comes after unregistered, so cannot find/call stop
            logger.debug(('_on_app_service_unregistered(): Unable to find Service w/ ID: %s, ' % new) +
                         'probably already unregistered. This should be considered an implementation bug!??')


def bootstrap(options=None):
    # TODO: proper documentation of environment options
    global R3_INSTANCE  # not a big fan of using globals like this,
    # but it does work here as a sloppy singleton mechanism...
    if isinstance(R3_INSTANCE, RostraPlatform):
        logger.warn('bootstrap(): called after application already initialized. This is not supported.')
        return R3_INSTANCE
    R3_INSTANCE = m = RostraPlatform()
    if options is None:  # minimum definition of an environment is a dictionary; adding arguments for good measure
        options = {
            'args': sys.argv[1:]
        }
    m.main(options)
    return m
