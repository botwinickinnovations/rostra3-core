# encoding: utf-8
# cython: embedsignature=False, linetrace=False, profile=False, emit_code_comments=False
__compiler__ = 'cython'

import logging

from envisage.api import IApplication as _IApplication
from traits.api import HasTraits, Instance, DictStrAny, Str

from .util import scrub_id, class_name


# region Extension Management

class ModelsRegistryService(HasTraits):
    app = Instance(_IApplication)
    ext = Str
    _models = DictStrAny
    logger = Instance(logging.Logger)

    def __init__(self, application, extension_point, **traits):
        traits['app'] = application
        traits['ext'] = extension_point
        super(ModelsRegistryService, self).__init__(**traits)

    def __models_default(self):
        app = self.app  # type: _IApplication
        extensions = app.get_extensions(self.ext)
        return {scrub_id(k).lower(): v for k, v in extensions if k}

    def refresh(self):
        if self.logger:
            self.logger.debug('%s::refresh()', class_name(self, canonical=False))
        self.reset_traits(['_models'])

    def _ext_point_listener(self, registry, change_event):
        # added, removed = change_event.added, change_event.removed
        self.refresh()

    def start(self):
        app = self.app  # type: _IApplication
        # TODO: should we augment extension registry to return handles?
        app.add_extension_point_listener(self._ext_point_listener, self.ext)

    def stop(self):
        app = self.app  # type: _IApplication
        try:
            app.remove_extension_point_listener(self._ext_point_listener, self.ext)
        except ValueError:  # value error is listener reference doesn't exist--just ignore that
            pass
        pass

    def get(self, model_id):
        if not model_id:
            return None
        model_id = scrub_id(model_id).lower()
        return self._models.get(model_id)

# endregion
