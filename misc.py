def rostra_envisage_combined_versions():
    from . import version as rostra_version
    versions = [rostra_version]
    try:
        # noinspection PyProtectedMember
        from envisage._version import version as envisage_version
    except ImportError:
        envisage_version = None
    if envisage_version:
        versions.append('envisage-%s' % envisage_version)
    return '.'.join(versions)
