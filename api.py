from envisage.api import ServiceOffer, ExtensionPoint, IApplication
# noinspection PyUnresolvedReferences
from traits.api import *

from . import exconstants as extension_constants, version
from .plugins import Plugin, GuiPlugin
from .services import ModelsRegistryService
from .upstream.traits import ForcedPrefixSuffixStr
from .util import class_name
from .qt4.task_manager import ThreadedTaskWorker

# Aliases and access to an effective API
xc = extension_constants
class_name = class_name
Plugin = Plugin
GuiPlugin = GuiPlugin
ForcedPrefixSuffixStr = ForcedPrefixSuffixStr
ModelsRegistryService = ModelsRegistryService
ServiceOffer = ServiceOffer
ExtensionPoint = ExtensionPoint
IApplication = IApplication
version = version
ThreadedTaskWorker = ThreadedTaskWorker


# Define a set of runtime constants in addition to extension constants
# noinspection PyPep8Naming
class runtime_properties(object):
    def __init__(self):
        from . import core
        import psutil  # TODO: not sure if I want to keep psutil here--maybe defer to core for all interesting stuff?
        self.__core = core
        self.__psu = psutil

    @property
    def PLUGINS_DIR(self):
        return self.__core.R3_PLUGINS_DIR

    @property
    def BIN_DIR(self):
        return self.__core.R3_BIN_DIR

    @property
    def FOUNDATION_DIR(self):
        return self.__core.PATH

    @property
    def EFFECTIVE_APPLICATION_ID(self):
        return self.__core.EFFECTIVE_APPLICATION_ID

    @property
    def USER_HOME_DIRECTORY(self):
        return self.__core.USER_HOME_DIRECTORY

    @property
    def USER_NAME(self):
        return self.__core.USER_NAME

    @property
    def INSTANCE(self):
        return self.__core.R3_INSTANCE

    @property
    def GUI_ENABLED(self):
        return self.__core.GUI_ENABLED

    @property
    def OPERATING_SYSTEM(self):
        return self.__core.OPERATING_SYSTEM

    @property
    def LOGICAL_PROCESSORS(self):
        return self.__psu.cpu_count(logical=True)

    @property
    def PHYSICAL_PROCESSORS(self):
        return self.__psu.cpu_count(logical=False)

    @property
    def TOTAL_RAM(self):
        return self.__psu.virtual_memory().total


# runtime_constants / rc is not accessible as a class, only as a container for constants
runtime_properties = rp = runtime_properties()
