""" A view containing an interactive Python shell. """

# Standard library imports.
import logging

# # TODO: this should be generalized and live somewhere else so that we're sure we've got a consistent set of Qt Imports...??
# (assuming it matters, everything should be using PySide at the base layer no matter what...)
# # monkey patch pyface Qt libraries into Jupyter Shell Widget
# qtconsole.qt.QtCore = pyface.qt.QtCore
# qtconsole.qt.QtGui = pyface.qt.QtGui
# qtconsole.qt.QtGui.QFileDialog = pyface.qt.QtGui.QFileDialog
# qtconsole.qt.QtSvg = pyface.qt.QtSvg
# qtconsole.qt.QtGui.qApp = pyface.qt.QtGui.QApplication.instance()

# Major library imports
from ipykernel.kernelbase import Kernel as KernelBase
from jupyter_client.manager import KernelManager
from qtconsole.inprocess import QtInProcessKernelManager
from qtconsole.jupyter_widget import JupyterWidget
from qtconsole.rich_jupyter_widget import RichJupyterWidget

# Enthought library imports.
from envisage.api import IExtensionRegistry
from envisage.api import ExtensionPoint
from envisage.plugins.python_shell.api import IPythonShell
from envisage.plugins.ipython_shell.api import INamespaceView
from pyface.workbench.api import View
from pyface.api import GUI
from traits.api import Instance, Property, provides, Dict, Any

# Setup a logger for this module.
logger = logging.getLogger('jupyter-ips-shell')


@provides(IPythonShell)
class IPythonShellView(View):
    """ A view containing an IPython shell. """

    #### 'IView' interface ####################################################

    # The part's globally unique identifier.
    id = 'envisage.plugins.python_shell_view'

    # The part's name (displayed to the user).
    name = 'Python Console'

    # The default position of the view relative to the item specified in the
    # 'relative_to' trait.
    position = 'bottom'

    #### 'PythonShellView' interface ##########################################

    # The interpreter's namespace.
    namespace = Dict

    # The names bound in the interpreter's namespace.
    names = Property(depends_on="namespace")

    #### 'IPythonShellView' interface #########################################

    kernel_manager = Instance(KernelManager)
    kernel = Instance(KernelBase)
    shell = Instance(JupyterWidget)
    client = Any()

    def _init_kernel_manager(self):
        kernel_manager = QtInProcessKernelManager()
        kernel_manager.start_kernel(show_banner=False)
        kernel = self.kernel = kernel_manager.kernel
        kernel.gui = 'qt4'
        client = self.client = kernel_manager.client()
        client.start_channels()
        return kernel_manager

    def _external_start_kernel_manager(self):
        pass

    def _kernel_manager_default(self):
        return self._init_kernel_manager()

    #### 'IExtensionPointUser' interface ######################################

    # The extension registry that the object's extension points are stored in.
    extension_registry = Property(Instance(IExtensionRegistry))

    #### Private interface ####################################################

    # Banner.
    _banner = ExtensionPoint(id='envisage.plugins.ipython_shell.banner')

    # Bindings.
    _bindings = ExtensionPoint(id='envisage.plugins.python_shell.bindings')

    # Commands.
    _commands = ExtensionPoint(id='envisage.plugins.python_shell.commands')

    ###########################################################################
    # 'IExtensionPointUser' interface.
    ###########################################################################

    def _get_extension_registry(self):
        """ Trait property getter. """

        return self.window.application

    ###########################################################################
    # 'View' interface.
    ###########################################################################

    def create_control(self, parent):
        """ Creates the toolkit-specific control that represents the view. """
        shell = self.shell = RichJupyterWidget(parent, banner='\n'.join(self._banner))
        shell.kernel_manager = self.kernel_manager
        shell.kernel_client = self.client
        # shell.set_default_style('linux')
        # self.execute_command('%matplotlib inline')

        # self.shell = IPythonWidget(parent,
        #                            banner='\n'.join(self._banner),
        #                            interp=self.interpreter)

        # # Namespace contributions.
        # for bindings in self._bindings:
        #     for name, value in bindings.items():
        #         self.bind(name, value)
        #
        # for command in self._commands:
        #     try:
        #         self.execute_command(command)
        #     except Exception as e:
        #         logger.exception(
        #             "The command '%s' supplied to the Ipython shell "
        #             "plugin has raised an exception:\n%s" %
        #             (command, traceback.format_exc()))

        # Register the view as a service.
        self.window.application.register_service(IPythonShell, self)

        self.execute_command('%matplotlib inline')
        # self.bind('app', self.window.application)

        ns_view = self.window.application.get_service(INamespaceView)
        if ns_view is not None:
            self.on_trait_change(ns_view._on_names_changed, 'names')

        def try_set_focus():
            try:
                self.shell.SetFocus()
            except:
                # The window may not have been created yet.
                pass

        def set_focus():
            self.window.application.gui.invoke_later(try_set_focus)

        GUI.invoke_later(set_focus)

        return self.shell

    def destroy_control(self):
        """ Destroys the toolkit-specific control that represents the view.

        """

        super(IPythonShellView, self).destroy_control()

        # Remove the namespace change handler
        ns_view = self.window.application.get_service(INamespaceView)
        if ns_view is not None:
            self.on_trait_change(
                ns_view._on_names_changed, 'names', remove=True
            )

    ###########################################################################
    # 'PythonShellView' interface.
    ###########################################################################

    #### Properties ###########################################################

    def _get_names(self):
        """ Property getter. """

        return self.control.ipython0.magic_who_ls()

    #### Methods ##############################################################

    def bind(self, name, value):
        """ Binds a name to a value in the interpreter's namespace. """
        logger.info('Trying to bind "%s" to %s' % (name, value))
        kv = {name: value}
        self.kernel.shell.push(kv)
        # self.namespace[name] = value
        # try:
        #     self.kernel.shell.push(kv)
        # except:
        #     logger.info('Unable to bind variable to shell interpreter')

        return

    def execute_command(self, command, silent=True):
        """ Execute a command in the interpreter. """
        # self.kernel.shell.ev(command)  # , hidden
        self.kernel.do_execute(command, silent)
        self.trait_property_changed('namespace', [], self.namespace)

    def execute_file(self, path, hidden=True):
        """ Execute a command in the interpreter. """
        raise NotImplementedError()

    def lookup(self, name):
        """ Returns the value bound to a name in the interpreter's namespace."""
        return self.namespace[name]

#### EOF ######################################################################
