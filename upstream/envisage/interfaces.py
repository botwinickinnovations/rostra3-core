from envisage.i_service_registry import IServiceRegistry as _IServiceRegistry


# noinspection PyClassHasNoInit
class IServiceRegistry(_IServiceRegistry):
    """
    Add a few extensions to the IServiceRegistry interface to facilitate
    hooking in to start or stop services that benefit from or require lifecycle
    management without requiring that the user override the service registry implementation.
    """

    # noinspection PyMethodMayBeStatic
    def get_protocol_from_id(self, service_id):
        """
        Return the protocol for the service with the specified id.

        If no such service exists a 'ValueError' exception is raised.
        """
        pass

    # noinspection PyMethodMayBeStatic
    def resolve_service_from_id(self, service_id):
        """
        Return the resolved service object for the service with the given id.

        The default implementation (`get_service_from_id`) doesn't resolve
        if the object is a factory, it just uses the provided object; however,
        the default approach for ServiceOffers is always to provide a factory.

        This method performs similarly to `get_service_from_id` except that it
        guarantees that factories are resolved to objects using the same mechanisms
        otherwise used internally in the vanilla service registry implementation.
        """
        # TODO: should this more closely mimic 'get_services'? that might be a better match?
        pass

    pass
