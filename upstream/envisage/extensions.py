from envisage.service_registry import ServiceRegistry as _ServiceRegistry
from traits.api import provides

from .interfaces import IServiceRegistry


# noinspection PyClassHasNoInit
@provides(IServiceRegistry)
class ServiceRegistry(_ServiceRegistry):
    def get_protocol_from_id(self, service_id):
        """ Return the protocol for the service with the specified id. """
        try:
            protocol, obj, properties = self._services[service_id]
        except KeyError:
            raise ValueError('no service with id <%d>' % service_id)
        return protocol

    def resolve_service_from_id(self, service_id):
        """
        Return the resolved service object for the service with the given id.

        The default implementation (`get_service_from_id`) doesn't resolve
        if the object is a factory, it just uses the provided object; however,
        the default approach for ServiceOffers is always to provide a factory.

        This method performs similarly to `get_service_from_id` except that it
        guarantees that factories are resolved to objects using the same mechanisms
        otherwise used internally in the vanilla service registry implementation.
        """
        return self.get_service(self.get_protocol_from_id(service_id))

    pass
