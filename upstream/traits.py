from __future__ import absolute_import

from traits.api import Str


# region Trait Extensions / Modifications

class ForcedPrefixSuffixStr(Str):
    prefix = Str
    suffix = Str

    # TODO: verify that this works as intended...

    def validate(self, obj, name, value):
        val = super(ForcedPrefixSuffixStr, self).validate(obj, name, value)
        if self.prefix and not val.startswith(self.prefix):
            val = self.prefix + val
        if self.suffix and not val.endswith(self.suffix):
            val += self.suffix
        return val

# endregion
