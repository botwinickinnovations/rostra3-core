"""
The upstream module contains modifications or extensions to upstream code that
can or should be contributed back up to other projects. They live here to provide a
stable base for Rostra, but many of these changes should be phased out in favor of
improving upstream packages. (Some should remain here or be merged elsewhere in Rostra
because they don't make sense to send upstream, but those decisions are being deferred
for the moment).
"""
