from PIL import Image
from pyface.qt.QtGui import QImage, QPixmap


class _PilImageQt(QImage):
    def __init__(self, im):
        assert isinstance(im, Image.Image)
        if im.mode != 'RGBA':
            im = im.convert('RGBA')
        data = None
        try:
            data = im.tobytes("raw", "BGRA")
        except SystemError:
            # workaround for earlier versions
            r, g, b, a = im.split()
            im = Image.merge("RGBA", (b, g, r, a))
        self.__data = data or im.tobytes()
        # noinspection PyCallByClass
        QImage.__init__(self, self.__data, im.size[0], im.size[1], QImage.Format_ARGB32)


def image_to_qimage(image):
    return _PilImageQt(image)


def image_to_pixmap(image):
    # noinspection PyCallByClass
    return QPixmap.fromImage(_PilImageQt(image))
