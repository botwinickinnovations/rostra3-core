import logging

from pyface.qt.QtGui import QMessageBox, QFileDialog, QWidget, QVBoxLayout, QProgressBar, QLabel


# region Window / Utility Functions

def show_window(window, show=True, focus=True, raise_up=True):
    if show:
        window.show()
    if focus:
        window.activateWindow()
    if raise_up:
        window.raise_()


def confirm_dialog(parent, title, question, yes_fn, no_fn, dialog_type='warning'):
    if dialog_type == 'critical':
        mb = QMessageBox.critical
    elif dialog_type == 'warning':
        mb = QMessageBox.warning
    elif dialog_type == 'question':
        mb = QMessageBox.question
    else:
        mb = QMessageBox.question
    response = mb(parent, title, question, QMessageBox.Yes, QMessageBox.No)
    if response == QMessageBox.Yes:
        return yes_fn()
    else:
        return no_fn()


# noinspection PyShadowingBuiltins
def file_dialog_open(widget, title, filter=None):
    file_name, selected_filter = QFileDialog.getOpenFileName(widget, title, filter=filter)
    return file_name


def file_dialog_save(widget, title, type_title, type_filter):
    file_name, selected_filter = QFileDialog.getSaveFileName(widget, title,
                                                             filter=u'%s (%s)' % (type_title, type_filter))
    return file_name


def file_dialog_select_dir(widget, title):
    file_path = QFileDialog.getExistingDirectory(widget, title)
    return file_path


# endregion


# region UI Foundation Classes, Mixins, and Widget Bases

class QtDesignerUiWidgetMixin(object):
    def __init__(self):
        self.logger = logging.getLogger(self.__class__.__name__)
        self._custom_init()
        self._setup_ui()
        self._custom_ui()
        self._connect_actions()
        self._setup_signals()

    def _custom_init(self):
        raise NotImplementedError()

    def _setup_ui(self):
        # TODO: also handle translation stuff? (currently just disregard that...)
        # noinspection PyUnresolvedReferences
        self.setupUi(self)

    def _custom_ui(self):
        raise NotImplementedError()

    def _connect_actions(self):
        raise NotImplementedError()

    def _setup_signals(self):
        raise NotImplementedError()


# endregion


# region Widgets

class StatusBarProgressBar(QWidget):
    def __init__(self, *args, **kwargs):
        super(StatusBarProgressBar, self).__init__(*args, **kwargs)
        layout = self._layout = QVBoxLayout(self)
        self.setLayout(layout)

        bar = self._progressbar = QProgressBar()
        layout.addWidget(bar)

        label = self._label = QLabel()
        label.setText(u'-- No Running Tasks --')
        layout.addWidget(label)

        self.set_value(0)

    def set_minimum(self, minimum):
        self._progressbar.setMinimum(minimum)

    def set_maximum(self, maximum):
        self._progressbar.setMaximum(maximum)

    def set_value(self, value):
        if value == self._progressbar.minimum() or value == self._progressbar.maximum():
            self._progressbar.hide()
            self._label.show()
        else:
            self._label.hide()
            self._progressbar.setValue(value)
            self._progressbar.show()

    @property
    def not_running_text(self):
        return self._label.text()

    @not_running_text.setter
    def not_running_text(self, value):
        self._label.setText(value)

# endregion
