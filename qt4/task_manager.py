# noinspection PyCompatibility
from Queue import Queue

from pyface.qt import QtCore


class ThreadedTaskWorker(QtCore.QThread):
    progressUpdate = QtCore.Signal(str, float)
    workerFinished = QtCore.Signal(str)

    def __init__(self, key):
        """
        Base Class for Qt4 Threaded Task Workers. Background Tasks for the given application
        should extend this class, defining the `_run()` method to perform the actual work.
        This class is designed to work with the `SimpleTaskManager` to provide a basic mechanism
        for having calculation tasks occur on a separate thread from the user interface while
        providing feedback (as progress and/or finished events) to a Task Manager that will
        update the user interface.

        :param key: a string key that is used to identify a task; the `SimpleTaskManager` will
        not run multiple instances of the same key at the same time--so the key is meant to be
        unique to a task. So if the same task is being repeated, they may have the same key and
        the `SimpleTaskManager` will have them run in series. If each task is unique and there
        is no conflict with them running simultaneously, then tasks should have different keys.
        :type key: str
        """
        QtCore.QThread.__init__(self)
        self.key = key
        # self._run = run
        # self._args = args
        # self._kwargs = kwargs

    def progress(self, status_or_float, maximum=None):
        """
        This method can be passed into the work implemented in `_run()` to provide progress updates
        for the task manager.

        :param status_or_float: the current status of the task.
        This should EITHER be a float between 0.0 and 1.0 OR if a maximum is also specified, a value
        such that `status_or_float` / `maximum` == current status between 0.0 and 1.0. Both are provided
        so that it is trivial to provide progress updates for loops using the form "self.progress(i, I)".
        :type status_or_float: int|float
        :param maximum: optional maximum value for progress calculation. If not provided, it is assumed
        that the value of `status_or_float` is the current progress between 0.0 and 1.0. If provided,
        the value for `status_or_float` is divided by the provided maximum value
        :type maximum: int|float|None
        """
        if maximum is not None and status_or_float == maximum:
            status = 1.0
        elif maximum is None:
            status = status_or_float
        else:
            status = float(status_or_float) / float(maximum)
        self.progressUpdate.emit(self.key, status)

    def run(self):  # *args, **kwargs
        self.progress(0.0)  # always send 0.0 at start of task
        self._run()
        self.progress(1.0)  # always send 1.0 at end of task
        self.workerFinished.emit(self.key)

    def _run(self):
        raise NotImplementedError()


# TODO: there should be a minimal interface for task managers so that we're not hard-coding in this implementation
class SimpleTaskManager(QtCore.QObject):
    progressUpdate = QtCore.Signal(int, int)

    def __init__(self, max_tasks=3):
        """
        A simple task manager to operate in a Qt-derived application to manage background tasks / tasks
        that shouldn't (or it's just decided they won't) run in the main thread.

        This is called a simple task manager because it is very simple. It should probably be extended to
        have a few more features, but this works as a base implementation. It'll probably be improved in
        the future to have a few more features, but the idea is for it to be a very simple starting point
        to make it relatively painless to have calculation routines etc. that do not occupy the main UI thread.

        It is generally recommended that background calculation tasks make use of libraries that do not require
        or can release the Python GIL.

        :param max_tasks: the limit to the number of simultaneous tasks/threads. This should be selected
        smartly based on the number of processors in the system, types of expected tasks, user preferences, etc.
        :type max_tasks: int
        """
        QtCore.QObject.__init__(self)
        self.max_tasks = max_tasks
        self._queue = Queue()
        self._running_tasks = {}
        self._progress = {}

    def _emit_progress(self):
        running_tasks = len(self._progress)
        progress = int(100.0 * sum(self._progress.values()) / running_tasks) if self._progress else 0
        self.progressUpdate.emit(running_tasks, progress)

    @QtCore.Slot(str, float)
    def _on_task_progress_update(self, key, value):
        if key in self._progress:
            self._progress[key] = value
        self._emit_progress()

    @QtCore.Slot(str)
    def _on_task_finished(self, key):
        if key in self._progress:
            del self._progress[key]
        if key in self._running_tasks:
            del self._running_tasks[key]
        self._emit_progress()  # send task manager overall status update
        self.check_queue()  # trigger checking the queue

    def check_queue(self):
        if not self._queue.empty() and len(self._running_tasks) < self.max_tasks:
            task_key, task = self._queue.get()
            if task_key not in self._running_tasks:
                # create entries in local data structures
                self._running_tasks[task_key] = task
                self._progress[task_key] = 0.0
                # connect task signals to task manager slots
                task.progressUpdate.connect(self._on_task_progress_update)
                task.workerFinished.connect(self._on_task_finished)
                # start task
                task.start()
            else:
                # put it back in the queue if there is a reason why we can't repeat this task
                self._queue.put((task_key, task))
        pass

    def queue(self, task):
        assert isinstance(task, ThreadedTaskWorker)
        self._queue.put((task.key, task))
        self.check_queue()

    pass
