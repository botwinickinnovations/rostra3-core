try:
    from .upstream.envisage.plugins.jupyter_ips.plugin import JupyterIpythonIpsShellPlugin as _GuiShellPlugin

    GuiShellPlugin = _GuiShellPlugin  # use plugin directly since it is an updated plugin definition
except ImportError:  # fallback to envisage's python shell plugin, make sure to augment it to be a rostra plugin
    from envisage.plugins.python_shell.python_shell_plugin import PythonShellPlugin as _GuiShellPlugin
    from rostra3.misc import rostra_envisage_combined_versions as _version
    from rostra3.plugins import BasePlugin as _Plugin

    # noinspection PyClassHasNoInit
    class GuiShellPlugin(_GuiShellPlugin, _Plugin):
        name = 'Envisage Python Shell Plugin'
        version = _version()
