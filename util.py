# region (Optional) Cython Import

try:
    import cython
except ImportError:
    # noinspection PyPep8Naming
    class cython(object):
        def __call__(self, *args, **kwargs):
            if args:
                return args[0]
            return None

        def __getattr__(self, item):
            return self


    cython = cython()


# endregion


def scrub_id(s):
    # TODO: make this more complete and/or more efficient; this was just really quick and dirty
    return str(s).replace('\\', '_').replace('/', '_').replace('-', '_').replace('.', '_').replace(' ', '_'). \
        replace('|', '_')


def class_name(clazz, canonical=True):
    if not isinstance(clazz, type):
        clazz = type(clazz)
    if canonical:
        return '%s.%s' % (clazz.__module__, clazz.__name__)
    return clazz.__name__
