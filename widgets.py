# encoding: utf-8
# cython: embedsignature=False, linetrace=False, profile=False, emit_code_comments=False
__compiler__ = 'cython'

import logging

from envisage.ui.workbench.api import (WorkbenchApplication as _EnvisageWorkbenchApp, Workbench as _EnvisageWorkbench,
                                       WorkbenchWindow as _EnvisageWorkbenchWindow)
from pyface.api import (SplashScreen)
from pyface.i_about_dialog import IAboutDialog, MAboutDialog
from pyface.image_resource import ImageResource
from pyface.qt import QtCore, QtGui
from pyface.ui.qt4.action.status_bar_manager import StatusBarManager as _PyFaceStatusBarManager
from pyface.ui.qt4.dialog import Dialog
from traits.api import (Bool, Instance, List, provides, Unicode)

from .core import ID, ASSETS_DIR, RostraApplication
from .qt4.gui import StatusBarProgressBar


class PlatformCoreAssets(object):
    ICON_WINDOW = ImageResource('window.ico', search_path=[ASSETS_DIR])
    IMAGE_ABOUT = ImageResource('about.png', search_path=[ASSETS_DIR])


@provides(IAboutDialog)
class AboutDialog(MAboutDialog, Dialog):
    """ Quick, Modified AboutDialog Implementation """

    additions = List(Unicode)
    image = Instance(ImageResource, ImageResource('about'))

    # The HTML displayed in the QLabel.
    _DIALOG_TEXT = '''
    <html>
      <body>
        <div align="center">
          <table width="100%%" cellspacing="4" cellpadding="0" border="0">
            <tr>
              <td align="center">
              <p>
                <img src="%s" alt="">
              </td>
            </tr>
          </table>
        </div>
        <div align="left">
          <p>
          %s<br>
          <br>
          <!-- Python %%s<br> -->
          <!-- Qt %%s<br> -->
          </p>
        </div>
      </body>
    </html>
    '''

    ###########################################################################
    # Protected 'IDialog' interface.
    ###########################################################################

    def _create_contents(self, parent):
        label = QtGui.QLabel()

        if parent.parent() is not None:
            title = parent.parent().windowTitle()
        else:
            title = ""

        # Set the title.
        self.title = "About %s" % title

        # Load the image to be displayed in the about box.
        image = self.image.create_image()
        path = self.image.absolute_path

        # The additional strings.
        additions = '<br />'.join(self.additions)

        # Get the version numbers.
        # py_version = sys.version[0:sys.version.find("(")]
        # qt_version = QtCore.__version__

        # Set the page contents.
        label.setText(self._DIALOG_TEXT % (path, additions))  # , py_version, qt_version))

        # Create the button.
        buttons = QtGui.QDialogButtonBox()

        if self.ok_label:
            buttons.addButton(self.ok_label, QtGui.QDialogButtonBox.AcceptRole)
        else:
            buttons.addButton(QtGui.QDialogButtonBox.Ok)

        buttons.connect(buttons, QtCore.SIGNAL('accepted()'), parent, QtCore.SLOT('accept()'))

        lay = QtGui.QVBoxLayout()
        lay.addWidget(label)
        lay.addWidget(buttons)

        parent.setLayout(lay)


# noinspection PyClassHasNoInit
class StatusBarManager(_PyFaceStatusBarManager):
    """
    Augmented Status Bar Manager that also keeps a permanent progress bar shown in the window
    and links the progress bar to events published by the application task manager
    """
    application = Instance(RostraApplication)  # type: RostraApplication
    messages = [u'Ready']
    size_grip = True  # we'll insert a size grip by default... not critical, but why not?
    progress_bar = Instance(StatusBarProgressBar)  # type: StatusBarProgressBar

    def create_status_bar(self, parent):
        if self.status_bar is None:  # a bit redundant against the original code, but whatever...
            # use original code to make sure we have a status bar
            status_bar = super(StatusBarManager, self).create_status_bar(parent)

            # add a progress bar for our application task manager
            self.progress_bar = StatusBarProgressBar()
            status_bar.addPermanentWidget(self.progress_bar)

            # listen for update events from the task manager and update our messages and progress bar
            self.application.task_manager.progressUpdate.connect(self._update_status_progress)

        return self.status_bar

    @QtCore.Slot(int, int)
    def _update_status_progress(self, tasks, value):
        msg = u'Ready' if not tasks else (u'%d Tasks Running...' % tasks)
        self.messages = [msg]
        # TODO: messages are also set by tooltip type things? so this may look terrible, decide if we should scrub this
        self.progress_bar.set_value(value)


class WorkbenchWindow(_EnvisageWorkbenchWindow):
    def _status_bar_manager_default(self):
        return StatusBarManager(application=self.application)

    pass


# noinspection PyClassHasNoInit,PyAbstractClass
class Workbench(_EnvisageWorkbench):
    window_factory = WorkbenchWindow
    pass


class RostraWorkbenchApp(_EnvisageWorkbenchApp, RostraApplication):
    start_gui_event_loop = Bool(True)
    id = ID
    icon = PlatformCoreAssets.ICON_WINDOW
    name = 'Rostra Platform'  # name used for titlebars etc
    window_size = (1024, 768)  # default window size when no other information available

    workbench_factory = Workbench
    logger = logging.getLogger('workbench')

    def run(self):
        self.logger.debug('RostraWorkbench::run()')

        # Make sure the GUI has been created (so that, if required, the splash screen is shown).
        gui = self.gui

        # Start the application.
        if self.start():
            # Create and open the first workbench window.
            window = self.workbench.create_window(position=self.window_position, size=self.window_size)
            window.open()

            # We stop the application when the workbench has exited.
            self.workbench.on_trait_change(self._on_workbench_exited, 'exited')

            # Start the GUI event loop if needed.
            if self.start_gui_event_loop:
                gui.start_event_loop()  # THIS CALL DOES NOT RETURN UNTIL THE GUI IS CLOSED.

        return

    def _about_dialog_default(self):
        about_dialog = AboutDialog(
            parent=self.workbench.active_window.control,
            image=PlatformCoreAssets.IMAGE_ABOUT,
            additions=[
                'Additional Content for the About Dialog should go here...'
            ],
        )
        return about_dialog

    # noinspection PyMethodMayBeStatic
    def _splash_screen_default(self):
        return SplashScreen(image=PlatformCoreAssets.IMAGE_ABOUT, show_log_messages=True, log_level=logging.INFO)

    pass

# region Tasks API-based Examples

# class FileBrowserPane(TraitsDockPane):
#     """ A simple file browser pane. """
#
#     # TaskPane interface ###################################################
#
#     id = 'example.file_browser_pane'
#     name = 'File Browser'
#
#     # FileBrowserPane interface ############################################
#
#     # Fired when a file is double-clicked.
#     activated = Event
#
#     # The list of wildcard filters for filenames.
#     filters = List(Str)
#
#     # The currently selected file.
#     selected_file = File(osp.expanduser('~'))
#
#     # The view used to construct the dock pane's widget.
#     view = View(Item('selected_file',
#                      editor=FileEditor(dclick_name='activated',
#                                        filter_name='filters'),
#                      style='custom',
#                      show_label=False),
#                 resizable=True)
#
#
# class PythonScriptBrowserPane(FileBrowserPane):
#     """ A file browser pane restricted to Python scripts.
#     """
#
#     # TaskPane interface ###################################################
#
#     id = 'example.python_script_browser_pane'
#     name = 'Script Browser'
#
#     # FileBrowserPane interface ############################################
#
#     filters = ['*.py']
#
#
# class IPythonEditor(IEditor):
#     """ A widget for editing Python code. """
#
#     # IPythonEditor interface ############################################
#
#     # Object being editor is a file
#     obj = Instance(File)
#
#     # The pathname of the file being edited.
#     path = Unicode
#
#     # Should line numbers be shown in the margin?
#     show_line_numbers = Bool(True)
#
#     # Events ####
#
#     # The contents of the editor has changed.
#     changed = Event
#
#     # A key has been pressed.
#     key_pressed = Event(KeyPressedEvent)
#
#     ###########################################################################
#     # 'IPythonEditor' interface.
#     ###########################################################################
#
#     def load(self, path=None):
#         """ Loads the contents of the editor. """
#         pass
#
#     def save(self, path=None):
#         """ Saves the contents of the editor. """
#         pass
#
#     def select_line(self, lineno):
#         """ Selects the specified line. """
#         pass
#
#
# @provides(IPythonEditor)
# class PythonEditor(Editor):
#     """ The toolkit specific implementation of a PythonEditor.  See the
#     IPythonEditor interface for the API documentation.
#     """
#
#     # IPythonEditor interface ############################################
#
#     obj = Instance(File)
#
#     path = Unicode
#
#     dirty = Bool(False)
#
#     name = Property(Unicode, depends_on='path')
#
#     tooltip = Property(Unicode, depends_on='path')
#
#     show_line_numbers = Bool(True)
#
#     # Events ####
#
#     changed = Event
#
#     key_pressed = Event(KeyPressedEvent)
#
#     def _get_tooltip(self):
#         return self.path
#
#     def _get_name(self):
#         return osp.basename(self.path) or 'Untitled'
#
#     ###########################################################################
#     # 'PythonEditor' interface.
#     ###########################################################################
#
#     def create(self, parent):
#         self.control = self._create_control(parent)
#
#     def load(self, path=None):
#         """ Loads the contents of the editor.
#         """
#         if path is None:
#             path = self.path
#
#         # We will have no path for a new script.
#         if len(path) > 0:
#             f = open(self.path, 'r')
#             text = f.read()
#             f.close()
#         else:
#             text = ''
#
#         self.control.code.setPlainText(text)
#         self.dirty = False
#
#     def save(self, path=None):
#         """ Saves the contents of the editor.
#         """
#         if path is None:
#             path = self.path
#
#         f = file(path, 'w')
#         f.write(self.control.code.toPlainText())
#         f.close()
#
#         self.dirty = False
#
#     def select_line(self, lineno):
#         """ Selects the specified line.
#         """
#         self.control.code.set_line_column(lineno, 0)
#         self.control.code.moveCursor(QtGui.QTextCursor.EndOfLine,
#                                      QtGui.QTextCursor.KeepAnchor)
#
#     ###########################################################################
#     # Trait handlers.
#     ###########################################################################
#
#     def _path_changed(self):
#         if self.control is not None:
#             self.load()
#
#     def _show_line_numbers_changed(self):
#         if self.control is not None:
#             self.control.code.line_number_widget.setVisible(
#                 self.show_line_numbers)
#             self.control.code.update_line_number_width()
#
#     ###########################################################################
#     # Private interface.
#     ###########################################################################
#
#     def _create_control(self, parent):
#         """ Creates the toolkit-specific control for the widget.
#         """
#         from pyface.ui.qt4.code_editor.code_widget import AdvancedCodeWidget
#         self.control = control = AdvancedCodeWidget(parent)
#         self._show_line_numbers_changed()
#
#         # Install event filter to trap key presses.
#         event_filter = PythonEditorEventFilter(self, self.control)
#         self.control.installEventFilter(event_filter)
#         self.control.code.installEventFilter(event_filter)
#
#         # Connect signals for text changes.
#         control.code.modificationChanged.connect(self._on_dirty_changed)
#         control.code.textChanged.connect(self._on_text_changed)
#
#         # Load the editor's contents.
#         self.load()
#
#         return control
#
#     def _on_dirty_changed(self, dirty):
#         """ Called whenever a change is made to the dirty state of the
#             document.
#         """
#         self.dirty = dirty
#
#     def _on_text_changed(self):
#         """ Called whenever a change is made to the text of the document.
#         """
#         self.changed = True
#
#
# class PythonEditorEventFilter(QtCore.QObject):
#     """ A thin wrapper around the advanced code widget to handle the key_pressed
#         Event.
#     """
#
#     def __init__(self, editor, parent):
#         super(PythonEditorEventFilter, self).__init__(parent)
#         self.__editor = editor
#
#     def eventFilter(self, obj, event):
#         """ Reimplemented to trap key presses.
#         """
#         if (self.__editor.control and obj == self.__editor.control and
#                     event.type() == QtCore.QEvent.FocusOut):
#             # Hack for Traits UI compatibility.
#             self.__editor.control.emit(QtCore.SIGNAL('lostFocus'))
#
#         elif (self.__editor.control and obj == self.__editor.control.code and
#                       event.type() == QtCore.QEvent.KeyPress):
#             # Pyface doesn't seem to be Unicode aware.  Only keep the key code
#             # if it corresponds to a single Latin1 character.
#             kstr = event.text()
#             try:
#                 kcode = ord(str(kstr))
#             except:
#                 kcode = 0
#
#             mods = event.modifiers()
#             self.key_pressed = KeyPressedEvent(
#                 alt_down=((mods & QtCore.Qt.AltModifier) ==
#                           QtCore.Qt.AltModifier),
#                 control_down=((mods & QtCore.Qt.ControlModifier) ==
#                               QtCore.Qt.ControlModifier),
#                 shift_down=((mods & QtCore.Qt.ShiftModifier) ==
#                             QtCore.Qt.ShiftModifier),
#                 key_code=kcode,
#                 event=event)
#
#         return super(PythonEditorEventFilter, self).eventFilter(obj, event)
#
#
# class ExampleTask(Task):
#     """ A simple task for editing Python code.
#     """
#
#     # Task interface #######################################################
#
#     id = 'example.example_task'
#     name = 'Multi-Tab Editor'
#
#     active_editor = Property(Instance(IEditor),
#                              depends_on='editor_area.active_editor')
#
#     editor_area = Instance(IEditorAreaPane)
#
#     menu_bar = SMenuBar(SMenu(TaskAction(name='New', method='new',
#                                          accelerator='Ctrl+N'),
#                               TaskAction(name='Open...', method='open',
#                                          accelerator='Ctrl+O'),
#                               TaskAction(name='Save', method='save',
#                                          accelerator='Ctrl+S'),
#                               id='File', name='&File'),
#                         SMenu(DockPaneToggleGroup(),
#                               id='View', name='&View'))
#
#     tool_bars = [SToolBar(TaskAction(method='new',
#                                      tooltip='New file',
#                                      image=ImageResource('document_new')),
#                           TaskAction(method='open',
#                                      tooltip='Open a file',
#                                      image=ImageResource('document_open')),
#                           TaskAction(method='save',
#                                      tooltip='Save the current file',
#                                      image=ImageResource('document_save')),
#                           image_size=(32, 32))]
#
#     ###########################################################################
#     # 'Task' interface.
#     ###########################################################################
#
#     def _default_layout_default(self):
#         return TaskLayout(
#             left=PaneItem('example.python_script_browser_pane'))
#
#     def activated(self):
#         """ Overriden to set the window's title.
#         """
#         return
#         filename = self.active_editor.path if self.active_editor else ''
#         self.window.title = filename if filename else 'Untitled'
#
#     def create_central_pane(self):
#         """ Create the central pane: the script editor.
#         """
#         self.editor_area = SplitEditorAreaPane()
#         return self.editor_area
#
#     def create_dock_panes(self):
#         """ Create the file browser and connect to its double click event.
#         """
#         browser = PythonScriptBrowserPane()
#         handler = lambda: self._open_file(browser.selected_file)
#         browser.on_trait_change(handler, 'activated')
#         return [browser]
#
#     ###########################################################################
#     # 'ExampleTask' interface.
#     ###########################################################################
#
#     def new(self):
#         """ Opens a new empty window
#         """
#         editor = PythonEditor()
#         self.editor_area.add_editor(editor)
#         self.editor_area.activate_editor(editor)
#         self.activated()
#
#     def open(self):
#         """ Shows a dialog to open a file.
#         """
#         dialog = FileDialog(parent=self.window.control, wildcard='*.py')
#         if dialog.open() == OK:
#             self._open_file(dialog.path)
#
#     def save(self):
#         """ Attempts to save the current file, prompting for a path if
#             necessary. Returns whether the file was saved.
#         """
#         editor = self.active_editor
#         try:
#             editor.save()
#         except IOError:
#             # If you are trying to save to a file that doesn't exist, open up a
#             # FileDialog with a 'save as' action.
#             dialog = FileDialog(parent=self.window.control,
#                                 action='save as', wildcard='*.py')
#             if dialog.open() == OK:
#                 editor.save(dialog.path)
#             else:
#                 return False
#         return True
#
#     ###########################################################################
#     # Protected interface.
#     ###########################################################################
#
#     def _open_file(self, filename):
#         """ Opens the file at the specified path in the editor.
#         """
#         editor = PythonEditor(path=filename)
#         self.editor_area.add_editor(editor)
#         self.editor_area.activate_editor(editor)
#         self.activated()
#
#     def _prompt_for_save(self):
#         """ Prompts the user to save if necessary. Returns whether the dialog
#             was cancelled.
#         """
#         dirty_editors = dict([(editor.name, editor)
#                               for editor in self.editor_area.editors
#                               if editor.dirty])
#         if not dirty_editors.keys():
#             return True
#         message = 'You have unsaved files. Would you like to save them?'
#         dialog = ConfirmationDialog(parent=self.window.control,
#                                     message=message, cancel=True,
#                                     default=CANCEL, title='Save Changes?')
#         result = dialog.open()
#         if result == CANCEL:
#             return False
#         elif result == YES:
#             for name, editor in dirty_editors.items():
#                 editor.save(editor.path)
#         return True
#
#     #### Trait change handlers ################################################
#
#     @on_trait_change('window:closing')
#     def _prompt_on_close(self, event):
#         """ Prompt the user to save when exiting.
#         """
#         close = self._prompt_for_save()
#         event.veto = not close
#
#     #### Trait property getter/setters ########################################
#
#     def _get_active_editor(self):
#         if self.editor_area is not None:
#             return self.editor_area.active_editor
#         return None
#
#     pass

# endregion
