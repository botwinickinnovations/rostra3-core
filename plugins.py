# encoding: utf-8
# cython: embedsignature=False, linetrace=False, profile=False, emit_code_comments=False
__compiler__ = 'cython'

import logging
import sys
from Queue import Queue
from collections import OrderedDict

from apptools.io import File
from envisage.api import Plugin as _Plugin
from envisage.plugin_manager import PluginEvent, PluginManager as _PluginManager
from traits.api import Bool, Directory, Event, List, Str, on_trait_change

from . import core  # FIXME: remove need to depend on core for GUI_ENABLED and other global variables
from . import exconstants as xc


# region Plugins, Plugin Manager, and related functions

# TODO: add starting, started, stopping, stopped events to plugin managers... (facilitate lifecycle hooks)

# noinspection PyClassHasNoInit
class R3PackagePluginManager(_PluginManager):
    """
    Modified version of ETS Package Plugin Manager for Rostra Platform.

    Searches for plugins on the given 'plugin_path's. All items in 'plugin_path' are directory names and
    they are all added to 'sys.path' (if not already present). Each directory is then searched for
    plugins as follows:-

    a) If the package contains a 'plugins.py' module, then we import it and
    look for a callable 'get_plugins' that takes no arguments and returns
    a list of plugins (i.e. instances that implement 'IPlugin'!).

    b) If the package contains any modules named in the form 'xxx_plugin.py'
    then the module is imported and if it contains a callable 'XxxPlugin' it is
    called with no arguments and it must return a single plugin.
    """

    # logging instance for all instances of this class
    logger = logging.getLogger('plugin-manager')

    # Plugin manifest.
    PLUGIN_MANIFEST = 'plugins.py'
    PLUGIN_SUFFIX = '_plugin'
    PLUGIN_EXTENSIONS = ['.py', '.so', '.pyd']

    # A list of directories that will be searched to find plugins.
    plugin_path = List(Directory)

    # Fired when a plugin has been rejected from being added
    plugin_rejected = Event(PluginEvent)

    absolute_plugin_exclusion = Bool(False)
    """
    Boolean value to determine the behavior for plugins that are 'excluded'.

    The default ETS plugin functionality is that plugins are always excluded if they are
    on the exclude list regardless of whether they are on the include list. That matches
    absolute_plugin_exclusion == True.

    For Rostra, the default is that plugin exclusion is not absolute / does not override inclusion.
    That means you can do a pattern like:
    include = [...] (specific things) and exclude = ['*']
    which means that ONLY what matches the include list will be loaded.

    To explain it better... here is the implementation difference:

    if self.absolute_plugin_exclusion:
        return self._is_included(plugin_id) and not self._is_excluded(plugin_id)
    else:
        return self._is_included(plugin_id) or not self._is_excluded(plugin_id)
    """

    @on_trait_change('plugin_path[]')
    def _plugin_path_changed(self, obj, trait_name, removed, added):
        self._update_python_sys_path(removed, added)
        self.reset_traits(['_plugins'])

    def __plugins_default(self):
        plugins = self._harvest_plugins_in_packages(include_filter=True)

        # determine dependency order (as best as possible at least...)
        plugins, rejects = order_plugins_by_dependencies(plugins, logger=self.logger)

        # TODO: do we want to do something with the rejects?

        return plugins

    def start(self):
        super(R3PackagePluginManager, self).start()
        self.logger.info('Initializing...')  # force this into the logging chain after plugins are started

    def start_plugin(self, plugin=None, plugin_id=None):
        plugin = plugin or self.get_plugin(plugin_id)
        try:
            if plugin:
                self.logger.info('Starting "%s" plugin', plugin.name)
                plugin.activator.start_plugin(plugin)
                self.logger.debug('start_plugin(): finished starting plugin: %s [%s]', plugin.id, plugin.name)
            else:
                self.logger.error('start_plugin(): Unable to locate plugin "%s"?', plugin_id)
        except ImportError as e:
            self.logger.error('start_plugin(%s, %s): Import Error: %s', plugin.name, plugin.id, e)

    def initialize(self):
        return self._plugins

    @staticmethod
    def _get_plugins_module(package_name):
        """ Import 'plugins.py' from the package with the given name.

        If the package does not exist, or does not contain 'plugins.py' then
        return None.
        """
        # TODO: augment this method to use the 'PLUGIN_MANIFEST' trait
        try:
            module = __import__(package_name + '.plugins', fromlist=['plugins'])
        except ImportError:
            module = None
        return module

    def _harvest_package_plugins_by_manifest(self, package_name):
        # If the package contains a 'plugins.py' module, then we import it and
        # look for a callable 'get_plugins' that takes no arguments and returns
        # a list of plugins (i.e. instances that implement 'IPlugin'!).
        try:
            plugins_module = self._get_plugins_module(package_name)
            if plugins_module is not None:
                factory = getattr(plugins_module, 'get_plugins', None)
                if factory is not None:
                    plugins = factory()
                    return plugins
            return None
        except ImportError:
            return None

    def _harvest_package_plugins_by_files(self, package_name, package_directory):
        # Otherwise, look for any modules in the form 'xxx_plugin.py' and
        # see if they contain a callable in the form 'XXXPlugin' and if they
        # do, call it with no arguments to get a plugin!

        plugins = []
        self.logger.debug('_harvest_package_plugins_by_files(): Looking for plugins in %s' % package_directory)
        for child in File(package_directory).children or []:
            if child.name.endswith(self.PLUGIN_SUFFIX) and child.ext in self.PLUGIN_EXTENSIONS:
                # TODO: add support for plugins which are only provided as pyc or pyo files?
                # this requires making sure we don't multi-load if multiple files are found (i.e. py, pyc, pyo)
                try:
                    module = __import__(package_name + '.' + child.name, fromlist=[child.name])

                    atoms = child.name.split('_')
                    capitalized = [atom.capitalize() for atom in atoms]
                    factory_name = ''.join(capitalized)

                    factory = getattr(module, factory_name, None)
                    if factory is not None:
                        plugins.append(factory())
                except ImportError as e:
                    self.logger.debug('_harvest_package_plugins_by_files(): Import Error: %s', e)
        return plugins

    def _harvest_plugins_in_package(self, package_name, package_directory):
        """ Harvest plugins found in the given package. """

        self.logger.debug('_harvest_plugins_in_package(): name=%s, dir=%s', package_name, package_directory)
        # try looking for a plugin manifest first
        plugins = self._harvest_package_plugins_by_manifest(package_name)
        if plugins is None:
            plugins = self._harvest_package_plugins_by_files(package_name, package_directory)
        # self.logger.debug('Package="%s", Dir="%s", Plugins=%s', package_name, package_directory, plugins)
        return plugins

    def _harvest_plugins_in_packages(self, include_filter=True):
        """ Harvest plugins found in packages on the plugin path. """

        self.logger.debug('_harvest_plugins_in_packages(): ...')
        plugins = []
        # noinspection PyTypeChecker
        for dir_name in self.plugin_path:
            for child in File(dir_name).children or []:
                if child.is_package:
                    new_plugins = self._harvest_plugins_in_package(child.name, child.path)
                    if new_plugins:
                        for plugin in new_plugins:
                            self._add_plugin(plugin, plugins, include_filter)
        return plugins

    def _include_plugin_instance(self, plugin):
        if not self._include_plugin(plugin.id):
            return False

        # check if the plugin requires a GUI and we won't be providing one... (require skipping that plugin)
        if getattr(plugin, 'gui', True) and not core.GUI_ENABLED:
            return False
        return True

    def _include_plugin(self, plugin_id):
        if self.absolute_plugin_exclusion:
            return self._is_included(plugin_id) and not self._is_excluded(plugin_id)
        else:
            return self._is_included(plugin_id) or not self._is_excluded(plugin_id)

    def add_plugin(self, plugin, include_filter=False):
        return self._add_plugin(plugin, self._plugins, include_filter)

    def _add_plugin(self, plugin, plugins_list, include_filter=True):
        if not include_filter or self._include_plugin_instance(plugin):
            plugins_list.append(plugin)
            self.plugin_added = PluginEvent(plugin=plugin)
        else:
            self.plugin_rejected = PluginEvent(plugin=plugin)

    def remove_plugin(self, plugin):
        return self._remove_plugin(plugin, self._plugins)

    def _remove_plugin(self, plugin, plugins_list):
        plugins_list.remove(plugin)
        self.plugin_removed = PluginEvent(plugin=plugin)

    def _update_python_sys_path(self, removals, additions):
        """ Add/remove the given entries from sys.path. """
        for dir_name in removals:
            if dir_name in sys.path and dir_name != core.PATH and dir_name != core.R3_PLUGINS_DIR:
                self.logger.debug('_update_python_sys_path(): Removing "%s"', dir_name)
                sys.path.remove(dir_name)

        for dir_name in additions:
            if dir_name not in sys.path:
                self.logger.debug('_update_python_sys_path(): Adding "%s"', dir_name)
                sys.path.append(dir_name)

    pass


# noinspection PyClassHasNoInit
class R3PluginManager(R3PackagePluginManager):
    def _log_plugin_added(self, plugin):
        self.logger.info('Found Plugin: %s', plugin_name(plugin, show_group=self.logger.level >= logging.DEBUG))

    def _log_plugin_rejected(self, plugin):
        self.logger.debug('Rejected Plugin: %s', plugin_name(plugin))

    def _log_plugin_removed(self, plugin):
        self.logger.info('Removed Plugin: %s', plugin_name(plugin))

    def _plugin_added_fired(self, event):
        plugin = event.plugin
        self._log_plugin_added(plugin)

    def _plugin_removed_fired(self, event):
        plugin = event.plugin
        self._log_plugin_removed(plugin)

    def _plugin_rejected_fired(self, event):
        plugin = event.plugin
        self._log_plugin_rejected(plugin)


# noinspection PyClassHasNoInit
class BasePlugin(_Plugin):
    """
    An extended version of the envisage Plugin which contains some additional
    properties that are useful for making a legitimate plugin system with
    reporting to the user and some richer features. We're not going to get into
    a complex dependency framework (a la OSGi), but at least track a bit more information
    and be able to 'group' related plugins into a plugin 'set'.
    """

    # TODO: potentially add information to the plugin about its target application?
    # e.g. target_application = 'rostra.spreadsheet_application'
    # TODO: use potential target app info as part of plugin manager inclusion/exclusion rules?
    # then use target_application for include/exclude operations so that 3rd party plugins don't
    # have to have rules about their plugin IDs (as that was the existing quick and dirty way
    # to make it that pre-execution settings about plugins can be pre-defined for different applications)

    group = Str
    """
    arbitrary string that is associated with a group of plugins. Could be common package or common part of ID etc.
    """

    version = Str
    """
    version string (expected to follow semantic versioning [or similar] with periods separating succeeding
    parts of the version string where the parts are listed in significance order),
    e.g. 4.2.10 being major.minor.patch or 3.10.1141.19901212 being major.minor.build.longDate or something like that...
    """

    gui = Bool(True)
    """
    boolean variable for whether gui is required for this plugin, defaults to True.
    """

    depends_on = List
    """
    list of plugin ID strings for plugins on which this plugin depends. This is used for a very simple dependency
    management system for which plugins aren't loaded if their dependencies aren't met and the start/stop ordering
    is such that dependencies are always started before their dependents.
    """

    # preferences = List(contributes_to=xc.EXT_PREFERENCES)  # preferences are common with/without GUI

    def _group_default(self):
        """ Default value for group is the module name of the plugin class """
        return type(self).__module__


# noinspection PyClassHasNoInit
class Plugin(BasePlugin):
    preferences = List(contributes_to=xc.EXT_PREFERENCES)  # preferences are common with/without GUI


# noinspection PyClassHasNoInit
class GuiPlugin(Plugin):
    """
    Extended Definition of Plugin for Workbench GUI Plugins.

    Has predefined traits for:
    * views
    * perspectives
    * preference_pages
    * action_sets

    """
    gui = True

    views = List(contributes_to=xc.EXT_VIEWS)
    perspectives = List(contributes_to=xc.EXT_PERSPECTIVES)
    preference_pages = List(contributes_to=xc.EXT_PREFERENCES_PAGES)
    action_sets = List(contributes_to=xc.EXT_ACTION_SETS)


def plugin_name(plugin, show_group=True):
    # TODO: add some parameters or control or alternate function for short/log name, include version, etc.
    # TODO: make this always show an appropriate amount of information by checking the values etc
    if isinstance(plugin, BasePlugin) and show_group:
        return '%s v%s [%s]' % (plugin.name, plugin.version, plugin.group)
    elif isinstance(plugin, BasePlugin):
        return '%s v%s' % (plugin.name, plugin.version)
    elif isinstance(plugin, _Plugin):
        return '%s' % plugin.name
    return str(type(plugin))


# TODO: cache plugin dependencies information?
def plugin_dependencies(plugin):
    dependencies = getattr(plugin, 'depends_on', [])
    if plugin.id != xc.PLUGIN_APP_CORE:
        dependencies += [xc.PLUGIN_APP_CORE]
    else:  # if it is APP CORE, we'll short cut checking for GUI stuff (temporary work-around)
        return dependencies
    if getattr(plugin, 'gui', True) and plugin.id != xc.PLUGIN_APP_WORKBENCH:
        dependencies += [xc.PLUGIN_APP_WORKBENCH]
    return dependencies


def order_plugins_by_dependencies(plugins, existing_plugins=None, logger=None):
    if plugins and not existing_plugins:
        new_plugins = plugins
        existing_plugins = []
    elif plugins and existing_plugins:
        # a bit ridiculous, but the idea is that we want to be sure we isolated the new ones...
        # the definition of plugins while providing existing plugins is not well defined,
        # but presumably you'd expect it to be only the new plugins...
        new_plugins = set(plugins).union(set(existing_plugins)) - set(existing_plugins)
    else:
        return existing_plugins or []  # this is the fallback if plugins resolves to False

    remainder = set()
    stack = Queue()
    for plugin in new_plugins:
        stack.put(plugin)
        remainder.add(plugin.id)

    # note: this isn't efficient or pretty, but usually there won't be many plugins, so it's good enough...
    result = OrderedDict((p.id, p) for p in existing_plugins)
    rejects = []
    while not stack.empty():
        plugin = stack.get()
        dependencies = plugin_dependencies(plugin)
        # note: these are key lookups! (don't forget that!)
        if all(d in result for d in dependencies):  # easy case, all dependencies accounted for
            result[plugin.id] = plugin
            remainder -= {plugin.id}
        elif all(d not in remainder for d in dependencies):  # worst case, will never succeed, just give in...
            # TODO: maybe try to load/find plugins in the future? (hence duplication...)
            # result[plugin.id] = plugin
            rejects.append(plugin)  # add to rejects list because we can't figure out the matches for dependencies
            remainder -= {plugin.id}
            if logger:
                logger.warn('Skipping "%s" plugin because unable to resolve dependencies.', plugin.name)
        else:
            stack.put(plugin)  # put it back on the stack and wait...

    return result.values(), rejects

# endregion
