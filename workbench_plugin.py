from envisage.ui.workbench.workbench_plugin import WorkbenchPlugin as _WorkbenchPlugin

from rostra3.misc import rostra_envisage_combined_versions as _version
from rostra3.plugins import BasePlugin as _Plugin


# noinspection PyClassHasNoInit
class WorkbenchPlugin(_WorkbenchPlugin, _Plugin):
    name = 'Workbench Plugin'
    version = _version()
